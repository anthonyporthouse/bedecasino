var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var path = {
    node: 'node_modules/',
    foundation: 'foundation-sites/',
};

elixir(function(mix) {
    mix.sass(
        ['app.scss', 'admin.scss'],
        null,
        {
            includePaths: [
                elixir.config.assetsPath + '/sass/',
                path.node + path.foundation + '/scss/',
            ]
        }
    );

    mix.scripts(
        [
            // jQuery
            // path.node + 'jquery/dist/jquery.js',
            path.node + path.foundation + 'vendor/jquery/dist/jquery.min.js',

            // Foundation
            path.node + path.foundation + 'dist/foundation.min.js',

            // Admin JS
            elixir.config.assetsPath + '/js/admin.js',
        ],
        'public/js/admin.js',
        './'
    );

    mix.scripts(
        [
            path.node + 'jquery/dist/jquery.js',
            elixir.config.assetsPath + '/js/app.js'
        ],
        'public/js/app.js',
        './'
    );

    mix.version([
        'css/app.css',
        'js/app.js',
        'css/admin.css',
        'js/admin.js',
    ]);
});
