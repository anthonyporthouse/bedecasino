<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CasinoAdminTest extends TestCase
{
    use DatabaseTransactions;

    public function testAddCasino()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->visit('/admin/casinos/create')
            ->type('Test', 'name')
            ->type('Testing Opening Hours', 'opening_times')
            ->type(55, 'latitude')
            ->type(-1.4, 'longitude')
            ->press('Save')
            ->see('Casino Successfully Added');
    }

    public function testEditCasino()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->visit('/admin/casinos/1/edit')
            ->type('Test', 'name')
            ->type('Testing Opening Hours', 'opening_times')
            ->type(55, 'latitude')
            ->type(-1.4, 'longitude')
            ->press('Save')
            ->see('Casino Successfully Updated');
    }

    public function testDestroyCasino()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->visit('/admin/casinos')
            ->press('Delete')
            ->see('Casino Successfully Deleted');
    }
}
