<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginTest extends TestCase
{
    /**
     * Test login with user credentials
     *
     * @return void
     */
    public function testAuthenticateAsAdministrator()
    {
        $this
            ->visit('/admin')
            ->seePageIs('/login')
            ->see('Login')
            ->type('anthony@bedecasino.com', 'email')
            ->type('test', 'password')
            ->press('Login')
            ->seePageIs('/admin/casinos');
    }
}
