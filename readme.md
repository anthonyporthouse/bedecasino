# Bede Casino

## Installation instructions
To install and run the tests, and live site, the following procedures should be followed

### Prerequisites
+ Virtualbox, Vagrant, php and composer

### Installation
+ `$ cd PATH_TO/BedeCasino`
+ `$ composer install`
+ `$ composer exec homstead make`
+ Modify contents of `Homestead.yaml` to make paths match for folder mapping and set address to `bedecasino.app`
+ Prepend the line `192.168.10.10 bedecasino.app` to your hosts file
+ `$ vagrant up`
+ `$ vagrant ssh`
+ `$ cd bedecasino`
+ `$ php artisan migrate`
+ `$ php artisan db:seed` by default only an area around Newcastle will be populated.
A test user with the email `anthony@bedecasino.com` and password of `test` has also been created

### Testing
Running the automated test suite is a relatively simple process. Simply ssh into the virtual machine, change into the
project directory then run `$ composer exec phpunit`

This should output something similar to the following:
```
PHPUnit 4.8.26 by Sebastian Bergmann and contributors.

....

Time: 3.77 seconds, Memory: 14.00MB

OK (4 tests, 22 assertions)
```

## Using the Site
To use the site access it via `https://bedecasino.app` You will need to approve the self signed certificate error,
however if you don't you won't be able to use the javascript location services.
