@extends('layouts.app')

@section('title', 'Login')

@section('content')
<div class="login">
    <div class="medium-6 medium-offset-3 columns">
        <div class="panel">
            <div class="callout">
                <form role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="small-3 columns">
                            <label class="text-right middle" for="email">E-Mail Address</label>
                        </div>
                        <div class="columns">
                            <input {{ $errors->has('email') ? 'aria-describedBy="email-errors"' : '' }} id="email" type="email" name="email" value="{{ old('email') }}">

                            @if ($errors->has('email'))
                                <p id="email-errors">{{ $errors->first('email') }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="small-3 columns">
                            <label class="text-right middle" for="password">Password</label>
                        </div>
                        <div class="columns">
                            <input {{ $errors->has('password') ? 'aria-describedBy="password-errors"' : '' }} id="password" type="password" name="password">

                            @if ($errors->has('password'))
                                <p id="password-errors">{{ $errors->first('password') }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="medium-offset-3 columns">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> Remember Me
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="medium-offset-3 columns">
                            <div class="button-group expanded">
                                <button type="submit" class="button">
                                    Login
                                </button>

                                <a class="button secondary" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
