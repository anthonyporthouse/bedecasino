<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title') - Bede Casino</title>
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet" type="text/css" media="all">
</head>
<body>
@yield('content')

<script src="{{ elixir('js/app.js') }}"></script>
@yield('scripts')
</body>
</html>
