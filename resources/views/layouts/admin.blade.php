<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title') - Bede Casino</title>
    <link href="{{ elixir('css/admin.css') }}" rel="stylesheet" type="text/css" media="all">
</head>
<body>

@include('admin.nav')
@include('admin.flashes')

@yield('content')

<script src="{{ elixir('js/admin.js') }}"></script>
@yield('scripts')
</body>
</html>
