<div class="row">
    <div class="medium-6 columns">
        <div class="row">
            <div class="small-12 columns">
                <label>
                    Casino Name
                    <input type="text" name="name" placeholder="Casino Name" value="{{ old('name', $casino->name) }}">
                </label>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <label for="casino-address">Casino Address</label>

                <div class="input-group">
                    <input class="input-group-field" id="casino-address" type="text" placeholder="Casino Address">
                    <div class="input-group-button">
                        <button id="map-search" class="button">Search</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="small-6 columns">
                <label>
                    Latitude
                    <input name="latitude" id="latitude" type="number" min="-90" max="90" step="any" value="{{ old('latitude', $casino->latitude) }}">
                </label>
            </div>
            <div class="small-6 columns">
                <label>
                    Longitude
                    <input name="longitude" id="longitude" type="number" min="-180" max="180" step="any" value="{{ old('longitude', $casino->longitude) }}">
                </label>
            </div>
        </div>

        <div class="row">
            <div class="small-12 columns">
                <label>
                    Opening Times
                    <textarea name="opening_times" id="opening-times" rows="4">{{ old('opening_times', $casino->opening_times) }}</textarea>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="small-12 columns">
                <div class="button-group expanded">
                    <button type="submit" class="button primary">Save</button>
                    <a href="{{ route('admin.casinos.index') }}" class="button alert">Cancel</a>
                </div>
            </div>
        </div>
    </div>

    <div class="medium-6 columns">
        <div style="height: 100%;" class="map"></div>
    </div>
</div>
