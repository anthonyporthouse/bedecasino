@extends('layouts.admin')

@section('title', 'Edit Casino')

@section('content')
    <div class="row">
        <div class="small-12 columns">
            <h1>Edit Casino</h1>
        </div>
    </div>

    <form method="post" action="{{ route('admin.casinos.update', $casino->id) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        @include('admin.casinos._form')
    </form>
@endsection

@section('scripts')
    @include('admin.casinos._form_scripts')
@endsection
