@extends('layouts.admin')

@section('title', 'All Casinos')

@section('content')
    <div class="row">
        <div class="small-12 columns">
            <h1>All Casinos</h1>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            {{ $casinos->links() }}
        </div>
    </div>

    <div class="row">
        @foreach($casinos as $casino)
            <div class="small-12 medium-6 columns">
                <div class="callout">
                    <div class="row">
                        <div class="large-6 columns">
                            <h2>{{ $casino->name }}</h2>

                            <form method="post" action="{{ route('admin.casinos.destroy', $casino->id) }}">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <div class="expanded small button-group">
                                    <a href="{{ route('admin.casinos.update', $casino->id) }}" class="warning button">Edit</a>
                                    <button class="alert button">Delete</button>
                                </div>
                            </form>
                        </div>
                        <div class="columns">
                            <div class="map"
                                 style="min-height: 12rem"
                                 data-latitude="{{ $casino->latitude }}"
                                 data-longitude="{{ $casino->longitude }}"></div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="row">
        <div class="small-12 columns">
            {{ $casinos->links() }}
        </div>
    </div>
@endsection

@section('scripts')
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnUYdBMGVjemRuVbLylwREx0CBZQHY330&callback=initMaps"
            type="text/javascript"></script>
    <script>
        "use strict";
        function initMaps() {
            Array.prototype.map.call(document.querySelectorAll('.map'), initMap);
        }

        function initMap(mapNode) {
            let latLng = {
                lat: Number(mapNode.dataset.latitude),
                lng: Number(mapNode.dataset.longitude)
            };

            let map = new google.maps.Map(mapNode, {
                zoom: 10,
                scrollwheel: false,
                draggable: false,
                center: latLng
            });

            let marker = new google.maps.Marker({
                position: latLng,
                map: map
            });
        }
    </script>
@endsection
