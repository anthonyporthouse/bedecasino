<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnUYdBMGVjemRuVbLylwREx0CBZQHY330&callback=initMaps"
        type="text/javascript"></script>
<script>
    "use strict";
    var initMaps = function() {
        let geocoder = new google.maps.Geocoder();

        let marker;

        Array.prototype.map.call(document.querySelectorAll('.map'), initMap);

        function searchForCasino(map, geocoder, address) {
            geocoder.geocode({
                address
            }, function (results, status) {
                if (status = google.maps.GeocoderStatus.OK) {
                    let location = results[0].geometry.location;

                    updateLocation({
                        lat: location.lat(),
                        lng: location.lng()
                    }, map);
                } else {
                    console.error(status);
                }
            });
        }

        function updateLocation(location, map) {
            if (!marker) {
                marker = new google.maps.Marker({
                    position: location,
                    map: map,
                    draggable: true
                });
            } else {
                marker.setPosition(location);
            }

            marker.addListener('dragend', function(e) {
                updateLocation({
                    lat: e.latLng.lat(),
                    lng: e.latLng.lng(),
                }, map)
            })

            map.panTo(location);

            document.getElementById('latitude').value = parseFloat(location.lat).toFixed(6);
            document.getElementById('longitude').value = parseFloat(location.lng).toFixed(6);
        }

        function initMap(mapEl) {
            let defaultLocation = {
                lat: Number(document.getElementById('latitude').value || 54.967948),
                lng: Number(document.getElementById('longitude').value || -1.621132)
            }

            let map = new google.maps.Map(mapEl, {
                zoom: 12,
                scrollwheel: false,
                center: defaultLocation
            });

            updateLocation(defaultLocation, map);

            map.addListener('click', function(e) {
                updateLocation({
                    lat: e.latLng.lat(),
                    lng: e.latLng.lng(),
                }, map)
            });

            document.getElementById('map-search').addEventListener('click', function(e) {
                e.preventDefault();
                searchForCasino(map, geocoder, document.getElementById('casino-address').value);
            })
        }
    };
</script>
