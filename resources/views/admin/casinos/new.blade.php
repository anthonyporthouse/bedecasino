@extends('layouts.admin')

@section('title', 'New Casino')

@section('content')
    <div class="row">
        <div class="small-12 columns">
            <h1>New Casino</h1>
        </div>
    </div>

    <form method="post" action="{{ route('admin.casinos.create') }}">
        {{ csrf_field() }}

        @include('admin.casinos._form')
    </form>
@endsection

@section('scripts')
    @include('admin.casinos._form_scripts')
@endsection
