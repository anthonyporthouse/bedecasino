<div class="top-bar">
    <div class="top-bar-title">
        <strong>Bede Casino</strong>
    </div>
     <div class="top-bar-left">
        <ul class="menu dropdown" data-dropdown-menu>
            <li>
                <a href="#">Casinos</a>
                <ul class="menu vertical">
                    <li><a href="{{ route('admin.casinos.index') }}">List Casinos</a></li>
                    <li><a href="{{ route('admin.casinos.create') }}">Add Casino</a></li>
                </ul>
            </li>
        </ul>
    </div>

    <div class="shrink top-bar-right">
        <ul class="dropdown menu align-right" data-dropdown-menu>
            <li>
                <a href="#">{{ Auth::user()->name }}</a>
                <ul class="menu vertical">
                    <li><a href="{{ url('/logout') }}">Log Out</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
