@if (session('success'))
    <div class="row">
        <div class="small-12 columns">
            <div class="callout success" data-closable>
                <button type="button" class="close-button" data-dismiss="alert" aria-label="Dismiss Alert" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Awesome!</strong> {{ session('success') }}
            </div>
        </div>
    </div>
@endif

@if (count($errors) > 0)
    <div class="row">
        <div class="small-12 columns">
            <div class="callout alert" data-closable>
                <button type="button" class="close-button" data-dismiss="alert" aria-label="Dismiss Alert" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>

                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif
