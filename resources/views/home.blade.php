@extends('layouts.app')

@section('title', 'Find a Casino')

@section('content')

    <div class="row">
        <div class="small-12 columns">
            <h1>Bede Casino</h1>
        </div>
    </div>

    <div class="row">
        <div class="medium-4 columns">
            <button id="use-current-location" class="button expanded">
                Use Current Location
            </button>

            <form id="search-address">
                <label for="address">
                    Find From Address
                </label>

                <div class="input-group">
                    <input class="input-group-field" id="address" name="address" type="text" placeholder="Address">
                    <div class="input-group-button">
                        <button class="button">Search</button>
                    </div>
                </div>
            </form>

        </div>
        <div class="medium-8 columns">
            <div class="flex-video widescreen map"></div>
        </div>
    </div>

@endsection

@section('scripts')
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnUYdBMGVjemRuVbLylwREx0CBZQHY330&callback=initMap"
            type="text/javascript"></script>

    <script>
        "use strict";

        let currentLocation;

        let markers = [];

        let map;

        let geocoder;

        // If geolocation is available, use it.
        if ('geolocation' in navigator) {
            $('body').on('click', '#use-current-location', function(e) {
                e.preventDefault();

                navigator.geolocation.getCurrentPosition(function(position) {
                    searchLocation(position.coords.latitude, position.coords.longitude);
                }, function() {}, {enableHighAccuracy: true});
            });

        } else {
            $('#use-current-location').remove();
        }

        $('body').on('submit', '#search-address', function(e) {
            e.preventDefault();

            geocodeAddress(document.getElementById('address').value);
        });

        function geocodeAddress(address) {
            if (!geocoder) {
                geocoder = new google.maps.Geocoder;
            }

            geocoder.geocode({
                address
            }, function (results, status) {
                if (status = google.maps.GeocoderStatus.OK) {
                    let location = results[0].geometry.location;

                    searchLocation(location.lat(), location.lng());
                } else {
                    console.error(status);
                }
            });
        }

        function searchLocation(lat, lng) {
            updateLocation({lat, lng}, map);

            $.getJSON('{{ route('casinos') }}', {
                lat: lat,
                lng: lng,
            }).done(dropPins);
        }

        // Map Functions
        function initMap() {
            let mapEl = document.querySelectorAll('.map')[0];
            let defaultLocation = {
                lat: Number(54.967948),
                lng: Number(-1.621132)
            };
            map = new google.maps.Map(mapEl, {
                zoom: 12,
                scrollwheel: false,
                center: defaultLocation
            });

            updateLocation(defaultLocation, map);
        }

        function dropPins(data) {
            markers.forEach(function(marker) {
                marker.setMap(null);
            });

            markers = [];

            data.forEach(function(entry, index) {
                let marker = new google.maps.Marker({
                    map: map,
                    position: {
                        lat: entry.latitude,
                        lng: entry.longitude,
                    },
                    title: entry.name,
                });

                let opening_times = (entry.opening_times || "").replace('\n', '<br>');

                let content = `<strong>${entry.name}</strong>
<div class="opening-times">${opening_times}</div>`;

                let infoWindow = new google.maps.InfoWindow({
                    content
                });

                marker.addListener('click', function() {
                    infoWindow.open(map, marker);
                });

                if (index === 0) {
                    map.panTo(marker.position);
                    infoWindow.open(map, marker);
                }

                markers.push(marker);
            });
        }

        function updateLocation(location, map) {
            map.panTo(location);
        }
    </script>
@endsection
