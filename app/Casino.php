<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Casino extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'longitude',
        'latitude',
        'opening_times'
    ];

    /**
     * Finds the casinos nearest to the given longitude and latitude, but returns a subset that is restricted to a
     * specific distance limit
     *
     * @param float $longitude
     * @param float $latitude
     * @param int $distanceLimit The maximum distance in KM that we should return
     *
     * @return Collection
     */
    public function findByNearest(float $longitude, float $latitude, int $distanceLimit = 50)
    {
        $casinos = DB::table('casinos')
            ->selectRaw(
                'id, name, opening_times, longitude, latitude,
                ROUND(ST_Distance_Sphere(POINT(?, ?), Point(longitude, latitude)) / 1000 ) distance',
                [
                    'long' => $longitude,
                    'lat' => $latitude
                ]
            )
            ->having('distance', '<=', $distanceLimit)
            ->orderBy('distance', 'ASC')
            ->get();

        return $casinos;
    }
}
