<?php

namespace App\Http\Controllers\Admin;

use App\Casino;
use App\Http\Requests\CasinoRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class Casinos extends Controller
{

    /**
     * Lists all casinos
     *
     * @return View
     */
    public function index()
    {
        $casinos = Casino::paginate(10);

        return view('admin.casinos.index', [
            'casinos' => $casinos
        ]);
    }

    /**
     * Shows the form to create a new Casino
     *
     * @return View
     */
    public function create()
    {
        return view('admin.casinos.new', ['casino' => new Casino]);
    }

    /**
     * Stores a new Casino into the system, then redirects you to the edit page for it
     *
     * @param CasinoRequest $request The validated Request data
     * @return RedirectResponse
     */
    public function store(CasinoRequest $request)
    {
        $casino = Casino::create([
            'name' => $request->get('name'),
            'latitude' => $request->get('latitude'),
            'longitude' => $request->get('longitude'),
            'opening_times' => $request->get('opening_times'),
        ]);

        return redirect()
            ->route('admin.casinos.update', $casino->id)
            ->with('success', 'Casino Successfully Added');
    }

    /**
     * Shows the form to edit a Casino
     *
     * @param int $id The ID of the casino to edit
     * @return View
     */
    public function edit(int $id)
    {
        $casino = Casino::find($id);

        return view('admin.casinos.edit', [
            'casino' => $casino
        ]);
    }

    /**
     * Updates the given casino then redirects you back to the edit page
     *
     * @param CasinoRequest $request The validated casino data
     * @param int $id The ID of the casino to update
     * @return RedirectResponse
     */
    public function update(CasinoRequest $request, int $id)
    {
        $casino = Casino::find($id);

        $casino->name = $request->get('name');
        $casino->latitude = $request->get('latitude');
        $casino->longitude = $request->get('longitude');
        $casino->opening_times = $request->get('opening_times');

        $casino->save();

        return redirect()
            ->route('admin.casinos.update', $casino->id)
            ->with('success', 'Casino Successfully Updated');
    }

    public function destroy(int $id)
    {
        Casino::destroy($id);

        return redirect()
            ->route('admin.casinos.index')
            ->with('success', 'Casino Successfully Deleted');
    }
}
