<?php

namespace App\Http\Controllers;

use App\Casino;
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Returns the nearest Casinos to the given long and lat, defaults to a 20KM Search
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findCasinos(Request $request)
    {
        $casinoModel = new Casino();
        $casinos = $casinoModel->findByNearest(
            $request->get('lng'),
            $request->get('lat'),
            $request->get('amount', 20)
        );

        return response()->json($casinos);
    }
}
