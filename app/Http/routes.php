<?php
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/** @var Router $router */
$router = app(Router::class);

$router->get('/', ['uses' => 'HomeController@index', 'as' => 'home']);
$router->get('/casinos', ['uses' => 'HomeController@findCasinos', 'as' => 'casinos']);

$router->auth();

$router->group([
    'namespace' => 'Admin',
    'prefix' => 'admin',
    'as' => 'admin.',
    'middleware' => ['auth'],
], function (Router $router) {
    $router->get('/', function() {
        return redirect()->route('admin.casinos.index');
    });

    $router->group([
        'prefix' => 'casinos',
        'as' => 'casinos.',
    ], function (Router $router) {
        $router->get('/', ['uses' => 'Casinos@index', 'as' => 'index']);
        $router->get('/create', ['uses' => 'Casinos@create', 'as' => 'create']);
        $router->post('/create', ['uses' => 'Casinos@store', 'as' => 'create']);
        $router->get('/{id}/edit', ['uses' => 'Casinos@edit', 'as' => 'update']);
        $router->put('/{id}/edit', ['uses' => 'Casinos@update', 'as' => 'update']);
        $router->delete('/{id}/destroy', ['uses' => 'Casinos@destroy', 'as' => 'destroy']);
    });
});
