<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasinosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casinos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->float('latitude', 8, 6);
            $table->float('longitude', 9, 6);
            $table->timestamps();
            $table->softDeletes();

            $table->index([
                'latitude',
                'longitude',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('casinos');
    }
}
