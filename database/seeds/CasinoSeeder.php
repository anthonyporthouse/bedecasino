<?php
use Illuminate\Database\Seeder;

class CasinoSeeder extends Seeder
{
    public function run()
    {
        \App\Casino::create([
            'name' => 'Anthony Porthouse Gaming',
            'longitude' => '-1.489331',
            'latitude' => '54.95759',
            'opening_times' => "Monday - Saturday: 9am - Late\nSunday: 10am - 9pm"
        ]);

        $faker = Faker\Factory::create('en_GB');
        for ($i = 0; $i < 100; $i++) {
            \App\Casino::create([
                'name' => $faker->company,
                'longitude' => $faker->longitude(-1.5074847, -1.8657153),
                'latitude' => $faker->latitude(54.8546553, 55.1273118),
                'opening_times' => "Monday - Saturday: {$faker->numberBetween(7, 12)}am - {$faker->numberBetween(5, 12)}pm"
            ]);
        }
    }
}
