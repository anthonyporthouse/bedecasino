<?php
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Anthony Porthouse',
            'email' => 'anthony@bedecasino.com',
            'password' => bcrypt('test'),
        ]);
    }
}
